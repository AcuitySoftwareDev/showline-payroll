Following is a list of features supported in each edition of showlinehrm
-------------------------------------------------------------------

### ShowlineHrmOpen Source Edition

![Employee Module](https://showlinehrm.s3.amazonaws.com/images/blog-images/advanced-employee-module.png)

#### Employee Management

 - Basic [Employee Management](https://showlinehrm.com) - Store, manage and retrieve employee information when required
 - Update all employee information without having to switch employees.
 - Search employee skills, qualifications and other information<br/>easily across whole company.
 - Terminate employees while keeping data in system.
 - Re-enable temporarily terminated employees with one click.
 - Employee archive feature to archive data of terminated employee

#### Other Features

 * [Company Information Management](https://showlinehrm.com) - Store and manage details about how companies, departments and branches of the organisation are connected
 * Time sheets - ShowlineHrmis a [timesheet app](https://showlinehrm.com) / [Open source timesheet management](https://showlinehrm.com) application to track time spent by employees on various projects
 * [Attendance Management](https://showlinehrm.com) - ShowlineHrmcan be used as a [attendance management system](https://showlinehrm.com) effectively for any size a company.
 * [LDAP Login](https://showlinehrm.com) - Users can share login with company LDAP server
 * [Travel Management](https://showlinehrm.com) - Module for managing travel requests

 
### ShowlineHrmPro Edition | [ShowlineHrm.com](https://showlinehrm.com/modules.php)

ShowlineHrmProfession version (in short ShowlineHrmPro) is the feature rich commercial alternative for showlinehrm
open source version.  ShowlineHrmPro supports following features
    
#### Leave Management

ShowlineHrm[Leave management system](https://showlinehrm.com) is only available in ShowlineHrmPro or Enterprise versions. ShowlineHrmleave module is a complete [leave management system](https://showlinehrm.com) for any type of a company

To learn more about leave management in showlinehrm refer:
- [Leave Admin Guide](http://blog.showlinehrm.com/docs/leave-admin)
- [Configuring Leave Module](http://blog.showlinehrm.com/docs/leave-setup)
- [Leave Rules](http://blog.showlinehrm.com/docs/leave-rules)

#### Audit Trial

Sometimes you need to access audit trail for your HRM system. Audit module records all the write actions (which alters your HRM system) 
of your employees in a quickly accessible and understandable manor. This help you to identify potential issues with the way employees
are using the system.

#### Expense Tracking

[Track Employee Expenses](https://showlinehrm.com) with expense management module.

You can learn more about [ShowlineHrmPro here](http://blog.showlinehrm.com/docs/showlinehrm-pro/)

To purchase ShowlineHrmPro please visit [https://showlinehrm.com/modules.php](https://showlinehrm.com/modules.php)

#### Training Management

Icehrm [training management system](https://showlinehrm.com) is for Module for managing courses, training sessions and employee attendance to training sessions.
 

### ShowlineHrmEnterprise Edition

In addition to pro version features ShowlineHrmenterprise cloud edition includes following features

#### [Employee History Management](https://showlinehrm.com)

#### [Payroll](https://showlinehrm.com)

ShowlineHrmEnterprise has a full featured payroll module including [PDF salary slip generation](https://showlinehrm.com)

#### Candidate / Recruitment Management

Recruitment module can be used as a [applicant tracking system](https://showlinehrm.com) or a [recruiting software](https://showlinehrm.com). ShowlineHrmrecruitment management system offers
following features

![Recruitment Job Position Sharing](https://showlinehrm.s3.amazonaws.com/images/blog-images/recruitment-share.png)
 
- Post jobs
- Let candidates apply for these jobs
- Schedule interviews
- Track candidate progress with notes
- Share job links with linkedIn, facebook, twitter and google+ directly from showlinehrm

![Candidate Details](https://showlinehrm.s3.amazonaws.com/images/blog-images/candidates.png)

More about [recruitment module](http://blog.showlinehrm.com/docs/recruitment/)