<?php
if(!defined('SIGN_IN_ELEMENT_MAPPING_FIELD_NAME')){define('SIGN_IN_ELEMENT_MAPPING_FIELD_NAME','employee');}

if(!defined('APP_NAME')){define('APP_NAME','Showline Hrm');}
if(!defined('FB_URL')){define('FB_URL', 'https://www.facebook.com/showlinehrm');};
if(!defined('TWITTER_URL')){define('TWITTER_URL', 'https://twitter.com/showlinehrmapp');};

if(!defined('HOME_LINK_ADMIN')){
    define('HOME_LINK_ADMIN', CLIENT_BASE_URL . "?g=admin&n=dashboard&m=admin_Admin");
}
if(!defined('HOME_LINK_OTHERS')){
    define('HOME_LINK_OTHERS', CLIENT_BASE_URL . "?g=modules&n=dashboard&m=module_Personal_Information");
}

//Version
define('VERSION', '1.1.20.SL');
define('CACHE_VALUE', '1.1.20.SL');
define('VERSION_NUMBER', '2300');
define('VERSION_DATE', '21/05/2021');

if(!defined('CONTACT_EMAIL')){define('CONTACT_EMAIL','showlinehrm@gamonoid.com');}
if(!defined('KEY_PREFIX')){define('KEY_PREFIX','ShowlineHrm');}
if(!defined('APP_SEC')){define('APP_SEC','dbcs234d2saaqw');}

define('UI_SHOW_SWITCH_PROFILE', true);
define('CRON_LOG', ini_get('error_log'));

define('MEMCACHE_HOST', '127.0.0.1');
define('MEMCACHE_PORT', '11211');

if(!defined('WK_HTML_PATH')){
    define('WK_HTML_PATH', '/usr/bin/xvfb-run -- /usr/local/bin/wkhtmltopdf');
}
define('ALL_CLIENT_BASE_PATH', '/var/www/showlinehrm.app/showlinehrmapp/');

define('LDAP_ENABLED', true);
define('RECRUITMENT_ENABLED', false);
define('APP_WEB_URL', 'https://showlinehrm.com');
