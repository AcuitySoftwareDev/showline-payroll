

function EmployeeLeaveCalendarAdapter(endPoint) {
	this.initAdapter(endPoint);
}

EmployeeLeaveCalendarAdapter.inherits(AdapterBase);



EmployeeLeaveCalendarAdapter.method('getDataMapping', function() {
	return [];
});

EmployeeLeaveCalendarAdapter.method('getHeaders', function() {
	return [];
});

EmployeeLeaveCalendarAdapter.method('getFormFields', function() {
	return [];
});


EmployeeLeaveCalendarAdapter.method('get', function(callBackData) {
});

EmployeeLeaveCalendarAdapter.method('getLeaveJsonUrl', function() {
	var url = this.moduleRelativeURL+"?a=ca&sa=getLeavesForMeAndSubordinates&t="+this.table+"&mod=modules%3Dleavecal";
	return url;
});


