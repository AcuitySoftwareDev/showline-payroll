<?php
ini_set('error_log', 'data/showlinehrm.log');

define('APP_NAME', 'Showline Hrm');
define('FB_URL', 'Showline Hrm');
define('TWITTER_URL', 'Showline Hrm');

define('CLIENT_NAME', 'app');
define('APP_BASE_PATH', '/opt/lampp/htdocs/showlinehrm-1.1.20.SL/core/');
define('CLIENT_BASE_PATH', '/opt/lampp/htdocs/showlinehrm-1.1.20.SL/app/');
define('BASE_URL','http://localhost/showlinehrm-1.1.20.SL/web/');
define('CLIENT_BASE_URL','http://localhost/showlinehrm-1.1.20.SL/app/');

define('APP_DB', 'showlinehrmdb');
define('APP_USERNAME', 'root');
define('APP_PASSWORD', '');
define('APP_HOST', 'localhost');
define('APP_CON_STR', 'mysqli://'.APP_USERNAME.':'.APP_PASSWORD.'@'.APP_HOST.'/'.APP_DB);

//file upload
define('FILE_TYPES', 'jpg,png,jpeg');
define('MAX_FILE_SIZE_KB', 10 * 1024);
