ShowlineHrm
===========

Installation
------------
 * Download the latest release https://github.com/AcuitySoftDev/showline-payroll/

 * Copy the downloaded file to the path you want to installShowline Hrm in your server and extract.

 * Create a mysql DB for and user. Grant all on Showline Hrm DB to new DB user.

 * Visit Showline Hrm installation path in your browser.

 * During the installation form, fill in details appropriately.

 * Once the application is installed use the username = admin and password = admin to login to your system.

 Note: Please rename or delete the install folder (<Showline hrm root>/app/install) since it could pose a security threat to yourShowline Hrm instance.

Manual Installation
-------------------




